package conditions.core.repository;

import conditions.core.model.draft.EscalationStep;

public interface EscalationStepRepository {

    void save(EscalationStep escalationStep);
}
