package conditions.core.repository;

import conditions.core.model.Fulfillment;

public interface FulfillmentRepository {
    void save(Fulfillment fulfillment);
}
