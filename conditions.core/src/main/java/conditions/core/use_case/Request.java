package conditions.core.use_case;

public record Request(
        String conditionId
) {
}
